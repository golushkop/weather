import axios from 'axios';
import Urls from '../models/urls';

export default class WeatherService {
  static geoDecoderApiByName(cityName) {
    let param = Urls.joinParam({
      address: cityName,
      result_type:'locality',
      language:'RU',
      key: 'AIzaSyA5Yck00UY2thbJ89vid71AdhnShYbnpew'
    });
    return encodeURI(`${'https://maps.google.com/maps/api/geocode/json'}?${param}`);
  }

  static weatherApi({latitude, longitude}, type='forecast') {
    let param = Urls.joinParam({
      lat: latitude,
      lon: longitude,
      units: 'metric',
      appid: 'c47144b70a4feaee5581f128636cf842'
    });
    return `http://api.openweathermap.org/data/2.5/${type}?${param}`;
  }

   static geoDecoderApiByCoords(latitude, longitude) {
    let param = Urls.joinParam({
      latlng:`${latitude},${longitude}`,
      result_type:'locality',
      language:'RU',
      key: 'AIzaSyA5Yck00UY2thbJ89vid71AdhnShYbnpew'
    });
    return encodeURI(`${'https://maps.google.com/maps/api/geocode/json'}?${param}`)
  }

  static async getWeather(city) {
      let url = WeatherService.weatherApi(city.location);
      let response = await axios.get(url);
      let result = response.data.list;
      return result
  }


  static async getCities(param) {
    // Получение информации о городах
      try {
          let url = WeatherService.geoDecoderApiByName(param);
          let response = await axios.get(url);
          let results = response.data.results;
          let cities = [];
          results.forEach(res => {
              cities.push({
                  cityName: res.address_components[0].long_name,
                  regionName: res.address_components[1].long_name,
                  country: res.address_components[2].long_name,
                  formatted_address: res.formatted_address,
                  location: {
                      latitude: res.geometry.location.lat,
                      longitude: res.geometry.location.lng
                  },
                  weather: {}
              })
          });
          return cities;
      } catch (e) {
        console.error(e);
          return 'Error trying to find cities'
      }
  }
  static async getCityByCoords(lat, long) {
      let url = WeatherService.geoDecoderApiByCoords(lat, long);
      let response = await axios.get(url);
      let result = response.data.results[0];
      return result;
  }

}