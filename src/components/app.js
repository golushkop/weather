import _ from 'lodash';
import React from 'react';
import store from '../store'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AutoComplete from 'material-ui/AutoComplete'
import WeatherService from '../services/weather-service'
import City_card from './city-card';
import FlatButton from 'material-ui/FlatButton'
import '../styles/app.scss'


class App extends React.Component {
    state = {
        showModal: false,
        foundCities: []
    };

    componentDidMount() {
        store.subscribe(() => this.forceUpdate());
        navigator.geolocation.getCurrentPosition(
          (pos)=>{
            this.getDefaultCityByCoords(pos.coords)
          },
          (err)=>{
            console.log(err);
          }
          );
    }

    async getDefaultCityByCoords(pos) {
        const defaultCity = await WeatherService.getCityByCoords(pos.latitude, pos.longitude);
        defaultCity['weather'] = [];
        defaultCity['location'] = {
            latitude: defaultCity['geometry']['location']['lat'],
            longitude: defaultCity['geometry']['location']['lng']
        };
        defaultCity['cityName'] = defaultCity['address_components'][0]['long_name'];
        defaultCity['country'] = defaultCity['address_components'][1]['long_name'];
        this.state.foundCities.push(defaultCity);
        this.state.showModal = true;
        this.forceUpdate();
    }


    async handleUpdateFindCity(value) {
        if (value.length > 3) {
            const cities = await WeatherService.getCities(value);
            this.setState({foundCities: cities || []});
        }
    }

    selectCity() {
        if (this.state.showModal) {
            this.state.showModal = false
        }
        let newAdd = {
            type: 'add',
            amount: 1,
            city: this.state.foundCities[0]
        };
        store.dispatch(newAdd);
    }
    cancelDefaultCity() {
        this.state.showModal = false;
        this.forceUpdate();
    }

    showModal() {
        let modal = null;
        if (this.state.showModal) {
            modal =
                <div>
                    <div className="modal-window">
                    </div>
                    <div className="modal">
                        <div>
                            Ваш город - {this.state.foundCities[0].formatted_address}
                        </div>
                        <div>
                            <FlatButton label='Подтвердить' onClick={this.selectCity.bind(this)}/>
                            <FlatButton label='Отменть' onClick={this.cancelDefaultCity.bind(this)}/>
                        </div>
                    </div>
                </div>
        }
        return modal
    }

    render() {
        const city = store.getState().cities;
        return (
                <MuiThemeProvider>
                    <div className="main-app">
                        <AutoComplete
                            id="addCities"
                            dataSource={
                                _.map(this.state.foundCities, x => {
                                    return x.formatted_address
                                })
                            }
                            onUpdateInput={this.handleUpdateFindCity.bind(this)}
                            fullWidth={true}
                            onNewRequest={this.selectCity.bind(this)}
                            hintText='Начните вводить название города'
                        />
                        {city.map((x, i) => <City_card city={x} key={i.toString()} index={i + 1}/>
                        )}
                        {this.showModal()}
                    </div>
                </MuiThemeProvider>
        )
    }
}

export default App;