import React from 'react';
import {Card, CardActions, CardHeader, CardText, CardMedia, CardTitle} from 'material-ui/Card';
import store from '../store'
import FlatButton from 'material-ui/FlatButton'
import WeatherService from '../services/weather-service'
import '../styles/city-card.scss'
import 'weather-icons/css/weather-icons.css'

const weatherIconDict = [
    {
        'icon': 'wi wi-day-cloudy',
        'weather': 'Clouds'
    },
    {
        'icon': 'wi wi-day-rain',
        'weather': 'Rain'
    },
    {
        'icon': 'wi wi-day-sunny',
        'weather': 'Clear'
    },
];
// Time in minutes
const timeToDeleteFromStorage = 10;


class City_card extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.getWeatherFromStorage(this.props.city)
        // this.getWeather(this.props.city);
    }

    deleteCity(city) {
        let del = {
            type: 'delete',
            amount: 1,
            city: city
        };
        store.dispatch(del)
    }

    getWeatherFromStorage(city) {
        let weatherStorage = localStorage.getItem(city.cityName);
        if (weatherStorage) {
            const newWeather = JSON.parse(weatherStorage);
            const storeDate = new Date(newWeather['date']);
            const date = new Date();
            if (date - storeDate < timeToDeleteFromStorage * 60 * 1000) {
                this.addWeatherToCity(city, newWeather)
            } else {
                localStorage.removeItem(city.cityName);
                this.getWeather(city)
            }

        } else {
            this.getWeather(city)
        }
    }

    async getWeather(city) {
        let weather = {};
        weather['data'] = await WeatherService.getWeather(city);
        weather['custom'] = {};
        weather['custom'] = await this.defineCustomWeatherValues(weather['data']);
        this.addToLocalStorage(city, weather)
    }

    addWeatherToCity(city, weather) {
        let addWeather = {
            type: 'addWeather',
            weather: weather,
            city: city,
        };
        store.dispatch(addWeather);
    }

    addToLocalStorage(city, weather) {
        let weatherDate = weather;
        let date = new Date();
        date = date.getFullYear() + '-' +  ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2) +
            ' ' + date.getHours() + ':' + date.getMinutes();
        weatherDate['date'] = date;
        let newWeather = {
            'data': weather['data'],
            'custom': weather['custom'],
            'date': date
        };
        newWeather = JSON.stringify(newWeather);
        localStorage.setItem(city.cityName, newWeather);
        this.addWeatherToCity(city, weather)
    }

    defineCustomWeatherValues(weather) {
        let customWeather = [];
        weather.forEach(x => {
            let date = new Date(x.dt_txt);
            date =  ('0' + date.getDate()).slice(-2) + '.' + ('0' + (date.getMonth() + 1)).slice(-2) + '.' + date.getFullYear().toString().slice(-2);
            if (customWeather.length > 0 && customWeather[customWeather.length - 1]['date'] === date) {
                if (customWeather[customWeather.length - 1]['temp_max'] < x.main.temp_max) {
                    customWeather[customWeather.length - 1]['temp_max'] = x.main.temp_max
                }
                if (customWeather[customWeather.length - 1]['temp_min'] > x.main.temp_min) {
                    customWeather[customWeather.length - 1]['temp_min'] = x.main.temp_min
                }
            } else if (customWeather.length < 5) {
                let weatherIcon = '';
                weatherIconDict.forEach(w => {
                    if (w.weather === x.weather[0].main) {
                        weatherIcon = w.icon;
                    }
                });
                customWeather.push({
                    date: date,
                    temp_max: x.main.temp_max,
                    temp_min: x.main.temp_min,
                    weather: x.weather[0].main,
                    weatherIcon: weatherIcon
                })
            }
        });
        return customWeather
    }

    generateWeatherDom() {
        let weatherDom = null;
        const weather = this.props.city.weather;
        if (weather['data'] && weather['data'].length > 0) {
            weatherDom = weather['custom'].map(x => {
                return (
                    <div className="weather-div" key={x.date}>
                        <div className="date">{x.date.slice(0, 5)}</div>
                        <div className="temperature">{x.temp_min.toFixed(0)} ... {x.temp_max.toFixed(0)}</div>
                        <div className="weather"><i className={x.weatherIcon}/></div>
                    </div>
                )

            });
        }
        return weatherDom
    }

    render () {
        let weatherDom = this.generateWeatherDom();
        let city = this.props.city;
        let subtitle = null;
        if (city.formatted_address.toString().split(',')[2]) {
            subtitle = <span>{city.formatted_address.toString().split(',')[1] + ',' + city.formatted_address.toString().split(',')[2]}</span>
        } else {
            subtitle = <span>{city.formatted_address.toString().split(',')[1]}</span>
        }
        return (
            <div className="city-card">
                <Card>

                    <CardMedia overlay={<div className="weather-block">{weatherDom}</div>}>
                        <CardTitle title={city.cityName}
                                    subtitle={subtitle} />
                        <img src={require("../images/Autumn.jpg")} alt=""/>
                    </CardMedia>
                    <CardActions>
                      <FlatButton label="Удалить" onClick={() => this.deleteCity(this.props.city.formatted_address)}/>
                      <FlatButton label="Обновить" onClick={() => this.getWeather(this.props.city)}/>
                    </CardActions>
                </Card>
            </div>
        )
    }
}
export default City_card;