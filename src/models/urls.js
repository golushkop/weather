import _ from 'lodash';

const {GOOGLE_MAP_API_KEY} = require('../config.json');

export default class Urls {
    static geoDecoderApiByName(cityName) {
    let param = Urls.joinParam({
      address: cityName,
      result_type:'locality',
      language:'RU',
      key: GOOGLE_MAP_API_KEY
    });
    return encodeURI(`${Urls.geoDecoderApi}?${param}`);
  }
  static geoDecoderApi = 'https://maps.google.com/maps/api/geocode/json';

  static joinParam(param) {
    return _.entries(param).map(([k,v])=>{return `${k}=${v}`}).join("&")
  }
}