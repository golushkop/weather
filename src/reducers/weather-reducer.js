
const initialState = {count: 0, cities: []};
const countReducer = function (state=initialState, action) {
    let newState = null;
    let counter = null;
    let cities = [];
    let newCities = [];

    switch (action.type) {
        case 'add':
            counter = state.count+1;
            cities = state.cities;
            cities.push(action.city);
            newState = {
                count: counter,
                cities: cities
            };
            return newState;
        case 'delete':
            counter = state.count-1;
            cities = state.cities;
            newCities = [];
            if (counter < 0) {
                counter = 0
            }
            let ind = 0;
            cities.forEach(x => {
                if (x.formatted_address !== action.city) {
                    newCities.push(x)
                }
                ind ++;
            });
            newState = {
                count: counter,
                cities: newCities
            };
            return newState;
        case 'addWeather':
            cities = state.cities;
            cities.forEach(x => {
                if (x.formatted_address === action.city.formatted_address) {
                    x['weather'] = action.weather;
                    x['date'] = action.date
                }
            });
            newState = {
                count: state.count,
                cities: cities
            };
            return newState;
        default:
            newState = initialState;
            return newState;
    }
};
export default countReducer