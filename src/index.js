import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import registerServiceWorkers from './registerServiceWorkers';
import App from './components/app';

export class Main extends React.Component {
    componentDidMount() {
        console.log('Hello')
    }

    render () {
        return (
            <App>
            </App>
        )
    }
}

ReactDOM.render(<Main />, document.getElementById('root'));
registerServiceWorkers();
